<?php
/**
 * @file
 * Title style.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Region with title'),
  'description' => t('Add a title to regions.'),
  'render region' => 'panels_title_style_render_region',
  'settings form' => 'panels_title_style_region_settings_form',
);

/**
 * Region settings form callback.
 *
 * @param array $style_settings
 */
function panels_title_style_region_settings_form($style_settings) {
  $settings_default_value = isset($style_settings['title']) ? $style_settings['title'] : '';
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $settings_default_value,
    '#description' => t('The title is filtered by filter_xss, so some simple HTML is allowed.'),
  );
  $form['always_show'] = array(
    '#type' => 'checkbox',
    '#title' => t('Always show title'),
    '#default_value' => isset($style_settings['always_show']) ? $style_settings['always_show'] : 0,
    '#description' => t('When this is set, the title is always shown even if the panel is empty.'),
  );
  return $form;
}
